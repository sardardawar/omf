let dataTableOptions = {
    lang : {
      "sProcessing":     "Procesando...",
      "sLengthMenu":     "Mostrar _MENU_ registros",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "NingÃºn dato disponible en esta tabla",
      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Ãšltimo",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
      },
      "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    },
    // dom : "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
    dom : "" +
    "<'row'<'col-sm-12 'tr>>" +
    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-3'l><'col-sm-12 col-md-4'p>>",
    length : 10,
  }
  
  var angularApp = angular.module('ptyAppOmf',['ngSanitize', 'ngCookies', 'datatables', 'ui.select'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('[{');
    $interpolateProvider.endSymbol('}]');
  })
  .directive('dateTimePicker', function($parse) {
    return {
      restrict: "A",
      require: "ngModel",
      link: function(scope, element, attrs, ngModelCtrl) {
        dtp = $(element).daterangepicker({
          autoUpdateInput: false,
        })
        dtp.on('apply.daterangepicker', function(ev, picker) {
          $(element).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
          ngModelCtrl.$setViewValue(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
          scope.$apply();
        });
      }
    }
  })
  .directive('initFieldValue', function ($parse) {
    return {
      require: '?ngModel',
      link: function (scope, element, attrs) {
        if (attrs.ngModel && attrs.value) {
          $parse(attrs.ngModel).assign(scope, attrs.value);
        }
        else if(attrs.ngModel && attrs.multiple && element[0].tagName == 'SELECT' && element[0].options.selectedIndex > -1 ) {
  
          var options = [];
  
          for(let i = 0; i < element[0].options.length; i++) {
            if(element[0].options[i].selected) {
              options.push(element[0].options[i].value)
            }
          }
  
          $parse(attrs.ngModel).assign(scope,options);
        }
        else if(attrs.ngModel &&  ! attrs.multiple && element[0].tagName == 'SELECT' && element[0].options.selectedIndex > -1 ) {
          $parse(attrs.ngModel).assign(scope,element[0].options[element[0].options.selectedIndex].value);
        }
        else if(attrs.ngModel && element[0].tagName == 'TEXTAREA' && element[0].textContent.length > 0 ) {
          $parse(attrs.ngModel).assign(scope,element[0].textContent);
        }
      }
    };
  })
  .directive('mask', ['$parse',  function($parse) {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, elm, attrs, ngModelCtrl) {
        elm.mask(attrs.mask, {completed: function() { 
          ngModelCtrl.$setViewValue(elm.val());  
          scope.$apply();
        }});                   
      }
    };
  }])
  .directive('showMask', function() {
    return {
      require: 'ngModel',
      link: function($scope, element, attrs, ngModelCtrl) {
        element.mask(attrs.showMask);
        ngModelCtrl.$parsers.unshift(function(value) {
          return element.cleanVal();
        });
        ngModelCtrl.$formatters.unshift(function(value) {
          return element.masked(value);
        });
      }
    };
  })
  .run(['$http', '$cookies', function($http, $cookies) {
    $http.defaults.headers.post['X-CSRFToken'] = $cookies.get('csrftoken');
  }]);