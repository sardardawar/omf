angularApp.controller('ptyControllerList', ['$rootScope', '$scope', '$http', '$compile', '$q', 'DTOptionsBuilder', 'DTColumnDefBuilder', function($rootScope, $scope, $http, $compile, $q, DTOptionsBuilder, DTColumnDefBuilder) {

    let ctrl = this;
    let th = angular.element('.pty-table-container table thead th[data-column]');
    
    ctrl.inputs = [];
    ctrl.SearchText = null;
    ctrl.lists = wLists.length > 0 ? JSON.parse(wLists) : [];
  
    th.each((i, e) => {
      var listValues = [];
      if( e.dataset.list != null && ctrl.lists[e.dataset.list] != null) {
        listValues = ctrl.lists[e.dataset.list];
      }
      ctrl.inputs.push({ index : i, label : e.innerHTML.trim(), type : e.dataset.type || 'input', column : e.dataset.column, list : listValues })
    })
  
    ctrl.dtInstance = null;
  
    ctrl.dtInstanceCallback = function(dtInstance) {
      ctrl.dtInstance = dtInstance;
      ctrl.addFilteringExtraFn();
    }
  
    ctrl.searchTable = function (event, obj){
      return;
      ctrl.dtInstance.DataTable.columns('[data-column="'+obj.column+'"]').search(ctrl.SearchText[obj.index]).draw();
    };
  
    ctrl.resetSearchFields = function() {
      ctrl.inputs.forEach((el, idx) => {
        if( ctrl.SearchText && ctrl.SearchText[el.index] != null ) { ctrl.SearchText[el.index] = ''; } 
        ctrl.dtInstance.DataTable.columns('[data-column="'+el.column+'"]').search('')
      })
      ctrl.dtInstance.DataTable.draw();
    }
  
    ctrl.searchInTable = function() {
      ctrl.dtInstance.DataTable.draw();
    }
  
    ctrl.toggleSearch = function() {
      ctrl.showSearch = ! ctrl.showSearch
      setTimeout(() => {
        ctrl.dtInstance.dataTable.fnDraw()
      },10)
    }
  
    ctrl.dtOptions = DTOptionsBuilder.newOptions()
    .withPaginationType('full_numbers')
    .withDisplayLength(dataTableOptions.length)
    .withLanguage(dataTableOptions.lang)
    .withDOM(dataTableOptions.dom)
    .withOption('deferRender', true)
    .withOption('scrollY', '60vh')
    .withOption('scrollX', true)
  
    ctrl.dtColumnDefs = [
      DTColumnDefBuilder.newColumnDef(1).withClass('pty-table-row-nowrap '),
      DTColumnDefBuilder.newColumnDef(14).withClass('pty-table-row-nowrap '),
      DTColumnDefBuilder.newColumnDef(16).withClass('pty-table-row-nowrap '),
  
    ];
  
    ctrl.addFilteringExtraFn = function() {
      if( ctrl.filterAdded ) return;
      ctrl.filterAdded = true;
      $.fn.dataTableExt.afnFiltering.push(
        function( oSettings, aData, iDataIndex ) {
          // Validate date time fields
          var show = true;
  
          ctrl.inputs.forEach((el, elIdx) => {
            
            if( ! ctrl.SearchText ) return true;
            if( ctrl.SearchText[elIdx] == null ) return true;
            if( ! ctrl.SearchText[elIdx].length > 0 ) return true; 
  
            var searchValue = ctrl.SearchText[elIdx].toLowerCase();
  
            if( el.type == 'datetime' ) {
              
              var dateRangeItems = ctrl.SearchText[elIdx].split(' - ');
                
              if( dateRangeItems.length > 0 ) {
                try {
                  var dateFrom = new Date(dateRangeItems[0] + ' 00:00:00'), dateTo = new Date(dateRangeItems[1] + ' 23:59:59');
                  var dateToEval = new Date(aData[el.index]);
                  if( ! (dateToEval.getTime() >= dateFrom.getTime() && dateToEval.getTime() <= dateTo.getTime())  ) {
                    show = false;
                  }
                }
                catch(e) {
                  console.error('Error converting string to date', e, dateRangeItems);
                }
  
              }
  
            }
            else if (el.type == 'select' ){
              console.log('select')
              if( aData[el.index].toLowerCase() != searchValue ) {
                show = false;
              }
            }
            else {
  
              if( aData[el.index].toLowerCase().indexOf(searchValue) < 0 ) {
                show = false;
              }
            } 
          })
  
          return show;
        }
      );
    }
  
  }])
  .controller('ptyControllerEdit', ['$rootScope', '$scope', '$http', '$compile', '$q',
  function($rootScope, $scope, $http, $compile, $q) {
    
    let ctrl = this;
  
    ctrl.getContainerColumnSize = function(name) {
      console.log(name)
      element = angular.element('.field-' + name);
      colSize = element.find('[ng-model]:eq(0)').attr('column-size') || 6;
      return colSize;
    }
  
    ctrl.submit = function() {
      $('form:eq(0)')[0].submit();
    }
    
  }]);  