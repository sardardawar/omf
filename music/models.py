from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.contrib.auth import get_user_model


class profileModel(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete= models.CASCADE)
    Email = models.EmailField(blank = True, null=True)
    Nombre = models.CharField(max_length=100)
    Apellido = models.CharField(max_length=100)
    Contraseña = models.CharField(max_length=100)
    def __str__(self):
        return 'Profile for user {}'.format(self.user.username)


from django.db import models

# Create your models here.

choice =(('Manual','Manual'),('Automatic','Automatic'))

class Compañía(models.Model):
    Código_de_Compañía_OWM                  = models.CharField(max_length=100)
    Código_de_Compañía_Emisora              = models.CharField(max_length=100)
    Nombre                                  = models.CharField(max_length=100)
    RUT                                     = models.CharField(max_length=100)
    Razón_Social                            = models.CharField(max_length=100)
    Giro                                    = models.CharField(max_length=100)
    Actividad_Económica_1                   = models.CharField(max_length=100)
    Actividad_Económica_2                   = models.CharField(max_length=100)
    Actividad_Económica_3                   = models.CharField(max_length=100)
    Actividad_Económica_4                   = models.CharField(max_length=100)
    Fono                                    = models.CharField(max_length=100)            
    Mail                                    = models.CharField(max_length=100)
    #Form 2
    Ciudad                                  = models.CharField(max_length=100)             
    Comuna                                  = models.CharField(max_length=100)
    Dirección                               = models.CharField(max_length=100)
    #Form 3
    Tipo_de_Generación                      = models.CharField(max_length=100,choices=choice,default='Automatic')
    Cantidad_de_Líneas_Guía                 = models.CharField(max_length=100)
    Tasa_IVA                                = models.CharField(max_length=100)
    Tipo_Código                             = models.CharField(max_length=100)
    Descripción_Producto                    = models.CharField(max_length=100)
    Descripción_Jerarquía_Producto          = models.CharField(max_length=100)
    URL_WebService                          = models.CharField(max_length=100)
    Usuario_WebService                      = models.CharField(max_length=100)
    Contraseña_WebService                   = models.CharField(max_length=100)
    Fecha_Creación                          = models.DateTimeField(auto_now_add=True)
    Creado_por                              = models.ForeignKey(User, on_delete=models.CASCADE)
    Fecha_Actualización                     = models.DateTimeField(auto_now=True)




