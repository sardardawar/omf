from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from .forms import loginForm, registerForm, EditProfileForm, profileInformForm, contactForm, CompañíaForm1
from django.contrib import messages
from django.contrib.auth.forms import UserChangeForm, PasswordChangeForm
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.conf import settings
from django.core.mail import EmailMessage
from .tokens import account_activation_token
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from .models import profileModel, Compañía
from django.urls import reverse
from django.db.models import Q


def handler404(request):
    return render(request, '404.html', status=404)


def home(request):
    return render(request, 'music/home.html', )


@login_required()
def dashboard(request):
    return render(request, 'music/dashboard.html')


def login_user(request):
    if request.method != 'POST':
        form = loginForm()
    else:
        form = loginForm(request.POST)
        if form.is_valid():
            user = authenticate(
                request, username=form.cleaned_data['username'], password=form.cleaned_data['password'])
            if user is not None:
                login(request, user)
                return redirect('home')
            else:
                messages.warning(
                    request, 'Usename or password may have been entered incorrectly.')
                return redirect('login')

    return render(request, 'music/login.html', {'form': form})


def logout_user(request):
    logout(request)
    return redirect('login')


@login_required()
def profile_user(request, user_name):
    message = ''
    try:
        user = User.objects.get(username=user_name)
        editProfile = False
        #print(request.user.username == user_name)
        if (request.user == user):
            if request.user.is_superuser:
                contactNumber = None
                editProfile = True
            else:
                contactNumber = profileModel.objects.get(
                    user=user).contactNumber
                editProfile = True
        else:
            if request.user.is_superuser:
                contactNumber = profileModel.objects.get(
                    user=user).contactNumber
                editProfile = False
            else:
                contactNumber = None
                editProfile = None
    except:
        user = request.user
        if request.user.is_superuser:
            contactNumber = None
            editProfile = True
            message = user_name + " Doest Not Exists "
        else:
            contactNumber = profileModel.objects.get(
                user=User.objects.get(username=request.user.username)).contactNumber
            editProfile = True
            message = user_name
    return render(request, 'music/profile.html', {'contactNumber': contactNumber, 'editProfile': editProfile, 'user': user, 'message': message})
    # return render(request, 'music/index-login.html')


def register_user(request):
    if request.method != 'POST':
        form = registerForm()
        form_2 = profileInformForm()
    else:
        form = registerForm(request.POST)
        form_2 = profileInformForm(request.POST)
        if form.is_valid() & form_2.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.set_password(form.cleaned_data['password2'])
            user.email = form.cleaned_data['email'].lower()
            user.save()
            profile = profileModel.objects.create(user=user)
            profile.contactNumber = form_2.cleaned_data['contactNumber']
            profile.save()
            current_site = get_current_site(request)
            message = render_to_string('music/acc_active_email.html', {
                'user': user, 'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                'token': account_activation_token.make_token(user),
            })
            # Sending activation link in terminal
            # user.email_user(subject, message)
            mail_subject = 'Activate your account.'
            #to_email = form.cleaned_data.get('email')
            to_email = form.cleaned_data.get('email').lower()
            email = EmailMessage(mail_subject, message, to=[to_email])
            email.send()
            # return HttpResponse('Please confirm your email address to complete the registration.')
            return render(request, 'music/acc_active_email_confirm.html')
    return render(request, 'music/register.html', {'form': form, 'form_2': form_2})


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        #login(request, user)
        # return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
        return redirect('login')
    else:
        return HttpResponse('Activation link is invalid!')


@login_required()
def edit_profile(request):
    if request.method != 'POST':
        form = EditProfileForm(instance=request.user)
    else:
        form = EditProfileForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            # return redirect('profile', username = request.user.username)
            return HttpResponseRedirect(reverse('profile', args=[request.user.username]))
    return render(request, 'music/edit_profile.html', {'form': form})


@login_required()
def change_password(request):
    if request.method != 'POST':
        form = PasswordChangeForm(user=request.user)
    else:
        form = PasswordChangeForm(data=request.POST, user=request.user)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return HttpResponseRedirect(reverse('profile', args=[request.user.username]))
    return render(request, 'music/change_password.html', {'form': form})


# -----------------------
def contact(request):
    if request.method != 'POST':
        form = contactForm()
    else:
        form = contactForm(request.POST)
        if form.is_valid():
            mail_subject = 'Contact -- By -- ' + \
                form.cleaned_data.get('userName')
            to_email = settings.EMAIL_HOST_USER
            message = form.cleaned_data.get('body')
            email = EmailMessage(mail_subject, message, to=[to_email])
            email.send()
            return redirect('home')

    context = {'form': form}
    return render(request, 'music/contact.html', context)


def CompañíaView(request):
    form1 = CompañíaForm1()

    if request.method == 'POST':
        form1 = CompañíaForm1(request.POST)

        if form1.is_valid():
            new = form1.save(commit=False)
            new.Creado_por = request.user
            form1.save()

            return redirect('compania')
    context = {
        'form1': form1,

    }
    return render(request, 'music/form.html', context)


def search(request):
    query = request.GET.get('query', None)
    company = Compañía.objects.all()
    if query is not None:
        company = company.filter(
            Q(Nombre__icontains=query) 
        )
    print(query)
    print(company)
    context = {

        'obj': company,
        'query': query
    }

    return render(request, 'music/compania.html', context)


def search1(request):
    query = request.GET.get('query', None)
    company = Compañía.objects.all()
    if query is not None:
        company = company.filter(

            Q(Código_de_Compañía_OWM__icontains=query)

        )
    context = {

        'obj': company,
        'query': query
    }

    return render(request, 'music/compania.html', context)


def search2(request):
    query = request.GET.get('query', None)
    company = Compañía.objects.all()
    if query is not None:
        company = company.filter(

            Q(Código_de_Compañía_Emisora__icontains=query)

        )
    context = {

        'obj': company,
        'query': query
    }

    return render(request, 'music/compania.html', context)

def search3(request):
    query = request.GET.get('query', None)
    company = Compañía.objects.all()
    if query is not None:
        company = company.filter(

            Q(RUT__icontains=query)

        )
    context = {

        'obj': company,
        'query': query
    }

    return render(request, 'music/compania.html', context)


def compania(request):
    obj = Compañía.objects.all()
    return render(request, 'music/compania.html', {'obj': obj})


def CompañíaViewEdit(request, id):
    obj = Compañía.objects.get(id=id)
    form1 = CompañíaForm1(instance=obj)
    if request.method == 'POST':
        form1 = CompañíaForm1(request.POST, instance=obj)
        if form1.is_valid():
            new = form1.save(commit=False)
            new.Creado_por = request.user
            form1.save()
            return redirect('compania')
    context = {
        'form1': form1,
        'obj': obj,
    }
    return render(request, 'music/formedit.html', context)
