from django.contrib import admin
from .models import profileModel,Compañía

class profileAdmin(admin.ModelAdmin):
    list_display = ['user', 'Nombre', 'Email']

admin.site.register(profileModel, profileAdmin)
admin.site.register(Compañía)